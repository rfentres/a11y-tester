FROM buildkite/puppeteer:7.1.0
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install --production --silent && mv node_modules ../
COPY . .
RUN chmod -R 777 ./results
EXPOSE 3000
CMD ["npm", "run", "start:prod"]

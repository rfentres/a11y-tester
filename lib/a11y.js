const {
    AxePuppeteer
} = require('@axe-core/puppeteer'); // Module for running axe scans in the puppeteer framework
const puppeteer = require('puppeteer'); // Module for automating browser interactions for testing
const fs = require('fs');
let codes = JSON.parse( fs.readFileSync('./public/status-codes.json', 'utf-8') );

const {
    pageCol,
    viewportCol,
    error,
    cliReporter,
    logResults
} = require('./console-format');

const launchConfig = {
    args: [
        // Required for Docker version of Puppeteer
        '--no-sandbox',
        '--disable-setuid-sandbox',
        // This will write shared memory files into /tmp instead of /dev/shm,
        // because Docker’s default for /dev/shm is 64MB
        '--disable-dev-shm-usage'
    ]
};

let throwIfNo = p => { throw new Error(`Missing parameter: ${p}`) }

const page = async ({ url = throwIfNo`url`, deviceName = 'Desktop', tags = ['wcag2a', 'wcag2aa'] }) => {
    //try {
    let result = {};
    const browser = await puppeteer.launch(launchConfig); // Launches a headless browser
    try {
        const page = await browser.newPage(); // Create a new page
        try {
            let device;
            if (deviceName === 'Desktop') {
                device = {
                    name: 'Desktop',
                    viewport: {
                        width: 1280,
                        height: 1024
                    },
                    userAgent: await browser.userAgent()
                }
            } else {
                device = puppeteer.devices[deviceName];
            }
            await page.emulate(device);

            await page.setBypassCSP(true);
            await page.setDefaultNavigationTimeout(0);
            const navigationPromise = page.waitForNavigation();  // Set a promise awaiting page navigation

            cliReporter(pageCol('Page: ' + url));
            cliReporter(viewportCol(device.name));
            let response = await page.goto(url, { waitUntil: 'domcontentloaded' }); // Load the page you want to test
            if (response.status() === 200) {
                await navigationPromise; // Wait for the page to load
                const builder = new AxePuppeteer(page); // Create instance of AxePuppeteer
                builder.withTags(tags);
    
                //var result = await builder.analyze(); // Check accessibility of page, returning axe results object
                result = await builder.analyze(); // Check accessibility of page, returning axe results object
                let title = await page.title();
                result.title = title || url;
                //result.error = null;
                logResults(result);
            } else {
                //console.log(response);
                const code = codes.find( el => (el.code == response.status()));
                const desc = code.description.replace(/^"|"$/g, '');
                console.error(`Couldn't scan ${response.url()}`);
                console.error(`${response.status()}: ${code.phrase} (${desc})`);

                return {
                    violations: [],
                    passes: [],
                    url: url,
                    title: url,
                    testEnvironment: {
                        windowWidth: 0,
                        windowHeight: 0
                    },
                    error: {
                        status: response.status(),
                        phrase: code.phrase,
                        desc: desc
                    }
                }
                // throw Error(`Couldn't scan ${response.url()}<br>
                //     ${response.status()}: ${code.phrase} (${desc})`)
            }
        } finally {
            await page.close();
        }
    } finally {
        await browser.close();
    }
    return result;

    // } catch (err) {
    //     console.log(err);
    //     throw Error('Problem scanning page');
    // }
}
exports.a11y = {
    page: page
}
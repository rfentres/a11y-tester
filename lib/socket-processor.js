const { combine, filterChecked, newId } = require('./utilities');
const { a11y } = require('./a11y');
const { throwIfNo } = require('./errors');

class SocketProcessor {
    constructor(db) {
        this.db = db;
        this.reporter = require('./reporter')(this.db);
        this.devices = this.db.getData(`/constants/devices`);
        this.tags = this.db.getData(`/constants/tags`);
        this.categories = this.db.getData(`/constants/categories`)
    }
    scanCancelled = (id = throwIfNo`id`) => {
        return (id in this.db.getData(`/cancelled`));
    }
    removeFromCancelled = id => {
        console.log(`removeFromCancelled(${id})`);
        if (this.scanCancelled(id)) this.db.delete(`/cancelled/${id}`);
    }
    getConfig = ({ devices = [], tags = [], categories = [] }) => {
        return {
            devices: filterChecked({ checked: devices, ary: this.devices, key: 'description' }),
            tags: filterChecked({ checked: tags, ary: this.tags, key: 'description' }),
            categories: filterChecked({ checked: categories, ary: this.categories, key: 'description' }),
        }
    }
    // startScanning = ({ reportId, data }) => {
    //     let timecode, scanId;
    //     timecode = scanId = data.id;
    //     let scan = this.db.getData(`/scans/${scanId}`);
    //     let config = this.getConfig(scan);
    //     this.db.push(`/reports/${reportId}`, {
    //         ...config,
    //         ...data
    //     });
    //     this.db.push(`/scans/${scanId}/lastRun`, timecode);
    //     return config;
    // }
    startScanning = ({ scanId }) => {
        let runId = newId();
        let timecode = runId;
        let scan = this.db.getData(`/scans/${scanId}`);
        let config = this.getConfig(scan);
        let numURLs = scan.urls.length;
        let numDevices = scan.devices.length || 1;
        let numActive = numURLs * numDevices;
        this.db.push(`/reports/${runId}`, {
            ...config,
            reportName: scan.reportName,
            id: scanId,
            status: "processing"
        });
        this.db.push(`/scans/${scanId}/lastRun`, timecode);
        return { config: config, runId: runId, numActive: numActive };
    }
    cancelScan = id => {
        console.log(`cancelScan(${id})`);
        this.db.delete(`/active/${id}`);
        this.db.push(`/cancelled/${id}/`, true);
        this.db.delete(`/reports/${id}`);
    }
    getPageHead = async ({ scanId, runId, url, device, i, tags }) => {
        //console.log(`getPageHead({${url}, ${device}, ${i}, ${scanId}})`);
        //try {
        let result = await a11y.page({ url: url, deviceName: device, tags: tags });
        this.db.push(`/reports/${runId}/results[]`, result);
        if (!this.scanCancelled(runId)) {
            if (typeof result.error !== 'undefined') {
                let err = `Couldn't scan ${url}<br>
                ${result.error.status}: ${result.error.phrase} (${result.error.desc})`;
                return {
                    error: err
                }
            } else {
                let head = this.reporter.getHead(runId, i);
                head.scan = scanId;
                return head;
            }
            // } else {
            //     return result
            // }
        }
        return false;
        // } catch(e) {
        //     throw Error(`Couldn't get page head`);
        // }
    }
    // getHead = async scanId => {
    //     let current = this.shiftActive(scanId);
    //     let head = await processor.getPageHead(current);
    //     if (head) socket.emit("displayPageHead", head);

    //     let result = await a11y.page({ url: current.url, device: current.device, tags: current.tags });
    //     if (!this.scanCancelled(scanId)) {
    //         this.db.push(`/reports/${scanId}/results[]`, result);
    //         let head = this.reporter.getHead(scanId, i);
    //         head.scan = scanId;
    //         return head;
    //     }
    //     return false;
    // }
    getRunConfig = scanId => {
        let scan = this.db.getData('/scans/' + scanId);
        return {
            id: scanId,
            urls: scan.urls,
            devices: filterChecked({ checked: scan.devices, ary: this.devices, key: 'description' }),
            tags: [...scan.tags, ...scan.categories],
            reportName: scan.reportName
        }
    }
    getCumulative = id => {
        return {
            stats: this.reporter.getSiteStats(id),
            wcag: this.reporter.wcag(id)
        }
    }
    getViewStats = ({ id, viewNum }) => {
        let stats = this.reporter.getViewStats(id, viewNum);
        stats.viewNum = viewNum;
        return stats;
    }
    prepareActive = ({ scanId, runId }) => {
        //let runId = this.newId();
        let config = this.getRunConfig(scanId);
        let merged = combine({ scanId: scanId, runId: runId, urls: config.urls, devices: config.devices, tags: config.tags });
        this.db.push(`/active/${runId}`, merged);
        return runId;
    }
    shiftActive = runId => {
        let activeRun = this.db.getData(`/active/${runId}`);
        if (activeRun.length > 0) {
            let active = this.db.getData(`/active/${runId}[0]`);
            this.db.delete(`/active/${runId}[0]`);
            return active;
        }
        return false;
    }
    numRemaining = runId => {
        let activeRun = this.db.getData(`/active/${runId}`);
        return activeRun.length;
    }
    scanSite = async ({ scanId, runId }) => {
        this.prepareActive({ scanId: scanId, runId: runId });
        let active = this.shiftActive(runId);
        let head = await this.getPageHead(active);
        head.numRemaining = this.numRemaining(runId)
        return head;
    }
    // newId = () => {
    //     return (new Date()).getTime(); // Use the time as the id
    // }
}
module.exports = SocketProcessor;
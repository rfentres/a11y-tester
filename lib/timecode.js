const addZero = (i) => {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

const formatTimecode = timecode => {
    let start = new Date(timecode);
    let year = start.getFullYear();
    let month = start.getMonth()+1; //index is zero-based
    let date = start.getDate();
    let h = addZero(start.getHours());
    let m = addZero(start.getMinutes());
    let s = addZero(start.getSeconds());
    return `${month}/${date}/${year} at ${h}:${m}:${s}`;
}
module.exports = formatTimecode;
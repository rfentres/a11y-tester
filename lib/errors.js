const throwIfNo = p => { throw new Error(`Missing parameter: ${p}`) }

module.exports = {
    throwIfNo: throwIfNo
}
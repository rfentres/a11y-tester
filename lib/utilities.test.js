const { combine, filterChecked } = require('./utilities');

describe('combine: combine urls and devices into array of objects', () => {
    let urls = ["https://vt.edu", "https://assist.vt.edu"];
    let devices = ["Desktop", "iPad landscape", "iPhone 5"];
    let tags = [ "wcag2a", "wcag2aa" ];
    test('where config ID is provided', () => {
        expect(combine({ id: 1614738740021, urls: urls, devices: devices, tags: tags })).toEqual([
                { i: 0, id: 1614738740021, url: "https://vt.edu", device: "Desktop", tags: [ "wcag2a", "wcag2aa" ] },
                { i: 1, id: 1614738740021, url: "https://vt.edu", device: "iPad landscape", tags: [ "wcag2a", "wcag2aa" ] },
                { i: 2, id: 1614738740021, url: "https://vt.edu", device: "iPhone 5", tags: [ "wcag2a", "wcag2aa" ] },
                { i: 3, id: 1614738740021, url: "https://assist.vt.edu", device: "Desktop", tags: [ "wcag2a", "wcag2aa" ] },
                { i: 4, id: 1614738740021, url: "https://assist.vt.edu", device: "iPad landscape", tags: [ "wcag2a", "wcag2aa" ] },
                { i: 5, id: 1614738740021, url: "https://assist.vt.edu", device: "iPhone 5", tags: [ "wcag2a", "wcag2aa" ] }
            ]);
    });
    test('where config ID is not provided', () => {
        expect(() => { combine({ urls: urls, devices: devices }) }).toThrow('Missing parameter: id');
    })
});

describe('filterChecked: return an array of values when supplied with from an array of objects based on supplied key', () => {
    let checked = ["wcag2a", "wcag2aa"];
    let ary = [ 
        { "id": "wcag2a", "description": "WCAG 2.0 Level A" },
        { "id": "wcag2aa", "description": "WCAG 2.0 Level AA" },
        { "id": "wcag21a", "description": "WCAG 2.1 Level A" }
    ];
    test('where id is provided', () => {
        expect(filterChecked({ checked: checked, ary: ary, key: 'id' })).toEqual(["wcag2a", "wcag2aa"]);
    });
    test('where description is provided', () => {
        expect(filterChecked({ checked: checked, ary: ary, key: 'description' })).toEqual(["WCAG 2.0 Level A", "WCAG 2.0 Level AA"]);
    });
    test('where key is not provided', () => {
        expect(filterChecked({ checked: checked, ary: ary })).toEqual(["WCAG 2.0 Level A", "WCAG 2.0 Level AA"]);
    });
    test('where array of checked items is not provided', () => {
        expect(filterChecked({ ary: ary, key: 'id' })).toEqual([]);
    });
    test('where array of objects is not provided', () => {
        expect(() => { filterChecked({ checked: checked, key: 'id' }) }).toThrow('Missing parameter: ary');
    });
    test('where neither array of checked items nor array of objects is provided', () => {
        expect(() => { filterChecked({ key: 'id' }) }).toThrow('Missing parameter: ary');
    });
    test('where empty object is provided', () => {
        expect(() => { filterChecked({}) }).toThrow('Missing parameter: ary');
    });
    test('where no object provided', () => {
        expect(() => { filterChecked() }).toThrow();
    });
});
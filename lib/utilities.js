const { throwIfNo } = require('./errors');
const combine = ({ scanId = throwIfNo`scanId`, runId = throwIfNo`runId`, urls = [], devices = ['Desktop'], tags = throwIfNo`tags` }) => {
    {
        let n = 0;
        let merged = [];
        for (let url of urls) {
            for (let device of devices) {
                merged.push({ scanId: scanId, runId: runId, i: n++, url: url, device: device, tags: tags })
            }
        }
        return merged;
    }
}
const filterChecked = ({ checked = [], ary = throwIfNo`ary`, key = 'description' }) => {
    let filtered = [];
    ary.forEach(item => {
        if (checked.indexOf(item.id) !== -1) {
            filtered.push(item[key]);
        }
    });
    return filtered;
}
const newId = () => {
    return (new Date()).getTime(); // Use the time as the id
}

module.exports = {
    combine: combine,
    filterChecked: filterChecked,
    newId: newId
};

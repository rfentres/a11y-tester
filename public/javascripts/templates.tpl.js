(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['charts'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-sm-6\"><div id=\"violationsChart"
    + alias4(((helper = (helper = lookupProperty(helpers,"i") || (depth0 != null ? lookupProperty(depth0,"i") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"i","hash":{},"data":data,"loc":{"start":{"line":3,"column":54},"end":{"line":3,"column":59}}}) : helper)))
    + "\"></div></div>\n        <div class=\"col-sm-6\"><div id=\"issuesChart"
    + alias4(((helper = (helper = lookupProperty(helpers,"i") || (depth0 != null ? lookupProperty(depth0,"i") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"i","hash":{},"data":data,"loc":{"start":{"line":4,"column":50},"end":{"line":4,"column":55}}}) : helper)))
    + "\"></div></div>\n    </div>\n</div>";
},"useData":true});
templates['code'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<figure><pre><code tabindex='0' class='language-html'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"html") || (depth0 != null ? lookupProperty(depth0,"html") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"html","hash":{},"data":data,"loc":{"start":{"line":1,"column":54},"end":{"line":1,"column":64}}}) : helper))) != null ? stack1 : "")
    + "</code></pre></figure>";
},"useData":true});
templates['config'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                <ul>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":11,"column":20},"end":{"line":13,"column":29}}})) != null ? stack1 : "")
    + "                </ul>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "                    <li>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</li>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "No Devices Configured";
},"6":function(container,depth0,helpers,partials,data) {
    return "No Tags Configured";
},"8":function(container,depth0,helpers,partials,data) {
    return "No Categories Configured";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"panel panel-default\">\n    <div class=\"panel-heading\">\n        <h2>Scan Configuration</h2>\n    </div>\n    <div class=\"panel-body\">\n        <div class=\"row\">\n            <div class=\"col-sm-4\">\n                <h3>Device Profiles</h3>\n"
    + ((stack1 = lookupProperty(helpers,"with").call(alias1,(depth0 != null ? lookupProperty(depth0,"devices") : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":16},"end":{"line":15,"column":25}}})) != null ? stack1 : "")
    + "                "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"devices") : depth0),{"name":"unless","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":16,"column":16},"end":{"line":16,"column":67}}})) != null ? stack1 : "")
    + "\n            </div>\n            <div class=\"col-sm-4\">\n                <h3>Standard / Purpose</h3>\n"
    + ((stack1 = lookupProperty(helpers,"with").call(alias1,(depth0 != null ? lookupProperty(depth0,"tags") : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":20,"column":16},"end":{"line":26,"column":25}}})) != null ? stack1 : "")
    + "                "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"tags") : depth0),{"name":"unless","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":16},"end":{"line":27,"column":61}}})) != null ? stack1 : "")
    + "\n            </div>\n            <div class=\"col-sm-4\">\n                <h3>Categories</h3>\n"
    + ((stack1 = lookupProperty(helpers,"with").call(alias1,(depth0 != null ? lookupProperty(depth0,"categories") : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":16},"end":{"line":37,"column":25}}})) != null ? stack1 : "")
    + "                "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"categories") : depth0),{"name":"unless","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":38,"column":16},"end":{"line":38,"column":73}}})) != null ? stack1 : "")
    + "\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['failure'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<li>\n    "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"message") || (depth0 != null ? lookupProperty(depth0,"message") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"message","hash":{},"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":2,"column":15}}}) : helper)))
    + "\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"relatedNodesMessage") || (depth0 != null ? lookupProperty(depth0,"relatedNodesMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"relatedNodesMessage","hash":{},"data":data,"loc":{"start":{"line":3,"column":4},"end":{"line":3,"column":29}}}) : helper))) != null ? stack1 : "")
    + "\n</li>";
},"useData":true});
templates['inst'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression(((helper = (helper = lookupProperty(helpers,"target") || (depth0 != null ? lookupProperty(depth0,"target") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"target","hash":{},"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":1,"column":10}}}) : helper)));
},"useData":true});
templates['instances'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <li class=\"list-group-item instance"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"index") || (depth0 != null ? lookupProperty(depth0,"index") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data,"loc":{"start":{"line":4,"column":47},"end":{"line":4,"column":56}}}) : helper)))
    + "\">\n                <p><b>Selector:</b></p> \n                "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"target") || (depth0 != null ? lookupProperty(depth0,"target") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"target","hash":{},"data":data,"loc":{"start":{"line":6,"column":16},"end":{"line":6,"column":28}}}) : helper))) != null ? stack1 : "")
    + "\n                "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"html") || (depth0 != null ? lookupProperty(depth0,"html") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"html","hash":{},"data":data,"loc":{"start":{"line":7,"column":16},"end":{"line":7,"column":26}}}) : helper))) != null ? stack1 : "")
    + "\n                "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"reasonHtml") || (depth0 != null ? lookupProperty(depth0,"reasonHtml") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"reasonHtml","hash":{},"data":data,"loc":{"start":{"line":8,"column":16},"end":{"line":8,"column":32}}}) : helper))) != null ? stack1 : "")
    + "\n            </li>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "        <ul class=\"list-group\">\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"nodes") || (depth0 != null ? lookupProperty(depth0,"nodes") : depth0)) != null ? helper : container.hooks.helperMissing),(options={"name":"nodes","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":12},"end":{"line":10,"column":22}}}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!lookupProperty(helpers,"nodes")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </ul>\n";
},"useData":true});
templates['insts'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "    x\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = (lookupProperty(helpers,"insts")||(depth0 && lookupProperty(depth0,"insts"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"instList") : depth0),{"name":"insts","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":3,"column":10}}})) != null ? stack1 : "");
},"useData":true});
templates['pageBody'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"panel-body\">\n    <div class=\"violations\"></div>\n</div>";
},"useData":true});
templates['pageHead'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"panel-heading\" id=\"heading"
    + alias4(((helper = (helper = lookupProperty(helpers,"i") || (depth0 != null ? lookupProperty(depth0,"i") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"i","hash":{},"data":data,"loc":{"start":{"line":1,"column":38},"end":{"line":1,"column":43}}}) : helper)))
    + "\">\n    <h4 class=\"panel-title\">\n        <button class=\"btn btn-default\" type=\"button\" data-toggle=\"collapse\" href=\"#collapse"
    + alias4(((helper = (helper = lookupProperty(helpers,"i") || (depth0 != null ? lookupProperty(depth0,"i") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"i","hash":{},"data":data,"loc":{"start":{"line":4,"column":92},"end":{"line":4,"column":97}}}) : helper)))
    + "\" aria-expanded=\"false\" aria-controls=\"collapse"
    + alias4(((helper = (helper = lookupProperty(helpers,"i") || (depth0 != null ? lookupProperty(depth0,"i") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"i","hash":{},"data":data,"loc":{"start":{"line":4,"column":144},"end":{"line":4,"column":149}}}) : helper)))
    + "\">\n            <span class=\"fas fa-plus-square\" aria-hidden=\"true\"></span> \n            <span class=\"fas fa-minus-square\" aria-hidden=\"true\"></span> \n            "
    + alias4(((helper = (helper = lookupProperty(helpers,"view") || (depth0 != null ? lookupProperty(depth0,"view") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"view","hash":{},"data":data,"loc":{"start":{"line":7,"column":12},"end":{"line":7,"column":20}}}) : helper)))
    + " "
    + alias4(((helper = (helper = lookupProperty(helpers,"windowSize") || (depth0 != null ? lookupProperty(depth0,"windowSize") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"windowSize","hash":{},"data":data,"loc":{"start":{"line":7,"column":21},"end":{"line":7,"column":35}}}) : helper)))
    + "\n            <span class=\"\"><span class=\"fas fa-heartbeat\"></span><span class=\"sr-only\">Health</span> "
    + alias4(((helper = (helper = lookupProperty(helpers,"score") || (depth0 != null ? lookupProperty(depth0,"score") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"score","hash":{},"data":data,"loc":{"start":{"line":8,"column":101},"end":{"line":8,"column":110}}}) : helper)))
    + "% </span> \n        </button>\n    </h4>\n</div>";
},"useData":true});
templates['pagePanel'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"panel panel-default\" id=\"panel"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"i") || (depth0 != null ? lookupProperty(depth0,"i") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"i","hash":{},"data":data,"loc":{"start":{"line":1,"column":42},"end":{"line":1,"column":47}}}) : helper)))
    + "\">\n</div>";
},"useData":true});
templates['pageTabPanel'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"panel-collapse collapse "
    + alias4(((helper = (helper = lookupProperty(helpers,"in") || (depth0 != null ? lookupProperty(depth0,"in") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"in","hash":{},"data":data,"loc":{"start":{"line":1,"column":36},"end":{"line":1,"column":42}}}) : helper)))
    + "\" id=\"collapse"
    + alias4(((helper = (helper = lookupProperty(helpers,"i") || (depth0 != null ? lookupProperty(depth0,"i") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"i","hash":{},"data":data,"loc":{"start":{"line":1,"column":56},"end":{"line":1,"column":61}}}) : helper)))
    + "\" aria-labelledby=\"heading"
    + alias4(((helper = (helper = lookupProperty(helpers,"i") || (depth0 != null ? lookupProperty(depth0,"i") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"i","hash":{},"data":data,"loc":{"start":{"line":1,"column":87},"end":{"line":1,"column":92}}}) : helper)))
    + "\">\n</div>";
},"useData":true});
templates['reasons'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<p class=\"summary\">\n    <ul class=\"failure-message\">\n        "
    + ((stack1 = (lookupProperty(helpers,"reasons")||(depth0 && lookupProperty(depth0,"reasons"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"reasonsList") : depth0),{"name":"reasons","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":8},"end":{"line":3,"column":44}}})) != null ? stack1 : "")
    + "\n    </ul>\n</p>";
},"useData":true});
templates['relatedList'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<ul>\n    <li>Related Nodes:\n        <ul class=\"list-group\">\n            "
    + ((stack1 = (lookupProperty(helpers,"related")||(depth0 && lookupProperty(depth0,"related"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"relatedNodeList") : depth0),{"name":"related","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":12},"end":{"line":4,"column":52}}})) != null ? stack1 : "")
    + "\n        </ul>\n    </li>\n</ul>";
},"useData":true});
templates['relatedNode'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<li class=\"list-group-item\">\n    <p><b>Selector:</b></p> \n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"targetString") || (depth0 != null ? lookupProperty(depth0,"targetString") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"targetString","hash":{},"data":data,"loc":{"start":{"line":3,"column":4},"end":{"line":3,"column":22}}}) : helper))) != null ? stack1 : "")
    + "\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"htmlString") || (depth0 != null ? lookupProperty(depth0,"htmlString") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"htmlString","hash":{},"data":data,"loc":{"start":{"line":4,"column":4},"end":{"line":4,"column":20}}}) : helper))) != null ? stack1 : "")
    + "\n</li>";
},"useData":true});
templates['rulePanel'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"panel panel-"
    + alias4(((helper = (helper = lookupProperty(helpers,"variation") || (depth0 != null ? lookupProperty(depth0,"variation") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"variation","hash":{},"data":data,"loc":{"start":{"line":1,"column":24},"end":{"line":1,"column":37}}}) : helper)))
    + "\">\n    <div class=\"panel-heading\"><h5 class=\"panel-title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"help") || (depth0 != null ? lookupProperty(depth0,"help") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"help","hash":{},"data":data,"loc":{"start":{"line":2,"column":55},"end":{"line":2,"column":63}}}) : helper)))
    + "\n        <span class=\"badge\">Issues: "
    + alias4(((helper = (helper = lookupProperty(helpers,"issues") || (depth0 != null ? lookupProperty(depth0,"issues") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"issues","hash":{},"data":data,"loc":{"start":{"line":3,"column":36},"end":{"line":3,"column":46}}}) : helper)))
    + "</span>\n    </h5></div>\n    <div class=\"panel-body\">\n        <p>"
    + alias4(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":6,"column":11},"end":{"line":6,"column":26}}}) : helper)))
    + " ( <a href=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"helpUrl") || (depth0 != null ? lookupProperty(depth0,"helpUrl") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"helpUrl","hash":{},"data":data,"loc":{"start":{"line":6,"column":38},"end":{"line":6,"column":49}}}) : helper)))
    + "\" target=\"_blank\">learn more</a> )</p>\n    </div>\n</div>";
},"useData":true});
templates['siteStats'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<table class=\"table table-striped\" style=\"width:auto; max-width:none\">\n    <tr>\n        <th scope=\"row\">Scan Started:</th>\n        <td>"
    + alias4(((helper = (helper = lookupProperty(helpers,"time") || (depth0 != null ? lookupProperty(depth0,"time") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"time","hash":{},"data":data,"loc":{"start":{"line":4,"column":12},"end":{"line":4,"column":20}}}) : helper)))
    + "</td>\n    </tr>\n    <tr>\n        <th scope=\"row\">Views evaluated:</th>\n        <td>"
    + alias4(((helper = (helper = lookupProperty(helpers,"numViews") || (depth0 != null ? lookupProperty(depth0,"numViews") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numViews","hash":{},"data":data,"loc":{"start":{"line":8,"column":12},"end":{"line":8,"column":24}}}) : helper)))
    + "</td>\n    </tr>\n    <tr>\n        <th scope=\"row\">Views with No Rule Violations:</th>\n        <td>"
    + alias4(((helper = (helper = lookupProperty(helpers,"viewsPassed") || (depth0 != null ? lookupProperty(depth0,"viewsPassed") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"viewsPassed","hash":{},"data":data,"loc":{"start":{"line":12,"column":12},"end":{"line":12,"column":27}}}) : helper)))
    + "</td>\n    </tr>\n    <tr>\n        <th scope=\"row\">Cumulative Health:</th>\n        <td>"
    + alias4(((helper = (helper = lookupProperty(helpers,"siteHealth") || (depth0 != null ? lookupProperty(depth0,"siteHealth") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"siteHealth","hash":{},"data":data,"loc":{"start":{"line":16,"column":12},"end":{"line":16,"column":26}}}) : helper)))
    + "%</td>\n    </tr>\n    <tr>\n        <th scope=\"row\">Number of Rule Violations:</th>\n        <td>"
    + alias4(((helper = (helper = lookupProperty(helpers,"numViolations") || (depth0 != null ? lookupProperty(depth0,"numViolations") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numViolations","hash":{},"data":data,"loc":{"start":{"line":20,"column":12},"end":{"line":20,"column":29}}}) : helper)))
    + "</td>\n    </tr>\n    <tr>\n        <th scope=\"row\">Violation Multiplier:</th>\n        <td>"
    + alias4(((helper = (helper = lookupProperty(helpers,"siteMultiplier") || (depth0 != null ? lookupProperty(depth0,"siteMultiplier") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"siteMultiplier","hash":{},"data":data,"loc":{"start":{"line":24,"column":12},"end":{"line":24,"column":30}}}) : helper)))
    + "</td>\n    </tr>\n</table>";
},"useData":true});
templates['stats'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <span class=\"label label-default\"><span class=\"fas fa-asterisk\"></span><span class=\"sr-only\">Violation Multiplier</span> "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"multiplier") || (depth0 != null ? lookupProperty(depth0,"multiplier") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"multiplier","hash":{},"data":data,"loc":{"start":{"line":6,"column":125},"end":{"line":6,"column":139}}}) : helper)))
    + "</span> \n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"stats\">\n    <p><b>Page:</b> <a href=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"url") || (depth0 != null ? lookupProperty(depth0,"url") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data,"loc":{"start":{"line":2,"column":29},"end":{"line":2,"column":36}}}) : helper)))
    + "\" target=\"_blank\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":2,"column":54},"end":{"line":2,"column":63}}}) : helper)))
    + "</a></p><p><span class=\"label label-default\"><span class=\"fas fa-heartbeat\"></span><span class=\"sr-only\">Health</span> "
    + alias4(((helper = (helper = lookupProperty(helpers,"score") || (depth0 != null ? lookupProperty(depth0,"score") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"score","hash":{},"data":data,"loc":{"start":{"line":2,"column":182},"end":{"line":2,"column":191}}}) : helper)))
    + "</span> \n    <span class=\"label label-success\">Rules Passed: "
    + alias4(((helper = (helper = lookupProperty(helpers,"numPasses") || (depth0 != null ? lookupProperty(depth0,"numPasses") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numPasses","hash":{},"data":data,"loc":{"start":{"line":3,"column":52},"end":{"line":3,"column":65}}}) : helper)))
    + "</span> \n    <span class=\"label label-danger\">Rules Violated: "
    + alias4(((helper = (helper = lookupProperty(helpers,"numViolations") || (depth0 != null ? lookupProperty(depth0,"numViolations") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numViolations","hash":{},"data":data,"loc":{"start":{"line":4,"column":53},"end":{"line":4,"column":70}}}) : helper)))
    + "</span> \n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"numViolations") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":7,"column":11}}})) != null ? stack1 : "")
    + "    <span class=\"label label-danger\" style=\"border: 1px solid orange\"># of Violations: "
    + alias4(((helper = (helper = lookupProperty(helpers,"numInstances") || (depth0 != null ? lookupProperty(depth0,"numInstances") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numInstances","hash":{},"data":data,"loc":{"start":{"line":8,"column":87},"end":{"line":8,"column":103}}}) : helper)))
    + "</span></p>\n</div>";
},"useData":true});
templates['target'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<li class=\"breadcrumb-item "
    + alias4(((helper = (helper = lookupProperty(helpers,"active") || (depth0 != null ? lookupProperty(depth0,"active") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"active","hash":{},"data":data,"loc":{"start":{"line":1,"column":27},"end":{"line":1,"column":37}}}) : helper)))
    + "\">\n    "
    + alias4(((helper = (helper = lookupProperty(helpers,"selector") || (depth0 != null ? lookupProperty(depth0,"selector") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"selector","hash":{},"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":2,"column":16}}}) : helper)))
    + "\n</li>";
},"useData":true});
templates['targets'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <li class=\"breadcrumb-item"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(data && lookupProperty(data,"last")),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":34},"end":{"line":3,"column":61}}})) != null ? stack1 : "")
    + "\">\n            "
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "\n            </li>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " active";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "<ul class=\"breadcrumb\">\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"targets") || (depth0 != null ? lookupProperty(depth0,"targets") : depth0)) != null ? helper : container.hooks.helperMissing),(options={"name":"targets","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":6,"column":16}}}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!lookupProperty(helpers,"targets")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</ul>";
},"useData":true});
templates['violation'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"panel panel-"
    + alias4(((helper = (helper = lookupProperty(helpers,"variation") || (depth0 != null ? lookupProperty(depth0,"variation") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"variation","hash":{},"data":data,"loc":{"start":{"line":1,"column":24},"end":{"line":1,"column":37}}}) : helper)))
    + " violation"
    + alias4(((helper = (helper = lookupProperty(helpers,"index") || (depth0 != null ? lookupProperty(depth0,"index") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data,"loc":{"start":{"line":1,"column":47},"end":{"line":1,"column":56}}}) : helper)))
    + "\">\n    <div class=\"panel-heading\"><h5 class=\"panel-title\">\n        <span class=\"fas fa-"
    + alias4(((helper = (helper = lookupProperty(helpers,"icon") || (depth0 != null ? lookupProperty(depth0,"icon") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data,"loc":{"start":{"line":3,"column":28},"end":{"line":3,"column":36}}}) : helper)))
    + "\"></span><span class=\"sr-only\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"impact") || (depth0 != null ? lookupProperty(depth0,"impact") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"impact","hash":{},"data":data,"loc":{"start":{"line":3,"column":67},"end":{"line":3,"column":77}}}) : helper)))
    + "</span> "
    + alias4(((helper = (helper = lookupProperty(helpers,"help") || (depth0 != null ? lookupProperty(depth0,"help") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"help","hash":{},"data":data,"loc":{"start":{"line":3,"column":85},"end":{"line":3,"column":93}}}) : helper)))
    + "\n        <span class=\"badge\">Issues: "
    + alias4(((helper = (helper = lookupProperty(helpers,"count") || (depth0 != null ? lookupProperty(depth0,"count") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"count","hash":{},"data":data,"loc":{"start":{"line":4,"column":36},"end":{"line":4,"column":45}}}) : helper)))
    + "</span>\n    </h5></div>\n    <div class=\"panel-body\">\n        <p>"
    + alias4(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":7,"column":11},"end":{"line":7,"column":26}}}) : helper)))
    + " ( <a href=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"helpUrl") || (depth0 != null ? lookupProperty(depth0,"helpUrl") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"helpUrl","hash":{},"data":data,"loc":{"start":{"line":7,"column":38},"end":{"line":7,"column":49}}}) : helper)))
    + "\" target=\"_blank\">learn more</a> )</p>\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"instances"),depth0,{"name":"instances","data":data,"indent":"        ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"reasonHtml") || (depth0 != null ? lookupProperty(depth0,"reasonHtml") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"reasonHtml","hash":{},"data":data,"loc":{"start":{"line":9,"column":8},"end":{"line":9,"column":24}}}) : helper))) != null ? stack1 : "")
    + "\n    </div>\n</div>";
},"usePartial":true,"useData":true});
templates['violations'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = (lookupProperty(helpers,"violations")||(depth0 && lookupProperty(depth0,"violations"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"violationList") : depth0),{"name":"violations","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":1,"column":44}}})) != null ? stack1 : "");
},"useData":true});
})();
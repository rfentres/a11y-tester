$(document).ready(function () {
    var socket = io('//' + document.location.hostname + ':' + document.location.port);
    
    const addZero = (i) => {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    const timeLogged = () => {
        let d = new Date();
        let h = addZero(d.getHours());
        let m = addZero(d.getMinutes());
        let s = addZero(d.getSeconds());
        let ms = d.getMilliseconds()
        return `${h}:${m}:${s}.${ms}`;
    }

    const showMessage = (msg, alertClass = "alert-info") => {
        $('#messages').append(`<p class="alert ${alertClass}">${msg}</p>`).find('p:last-child').delay(3000).fadeTo(400, 0, function () { this.remove(); });
        $('#log .list-group').append(`<li class="list-group-item list-group-item-info"><b>${timeLogged()} &gt;</b> ${msg}</li>`);
    }
    const error = msg => {
        $('#alerts').append(`<p class="alert alert-danger">${msg}</p>`).find('p:last-child').delay(3000).fadeTo(400, 0, function () { this.remove(); });
        console.error(msg);
        $('#log .list-group').append(`<li class="list-group-item list-group-item-danger"><b>${timeLogged()} &gt;</b> ${msg}</li>`);
    }

    socket.on('error', ({ msg }) => {
        error(msg);
    });
    nextFocus = function (sel, child, parentEl) {
        let nextSibling = $(sel).next();
        let prevSibling = $(sel).prev();
        let toFocus = $(parentEl);
        if (nextSibling.length > 0) {
            toFocus = (child !== null) ? nextSibling.find(child) : nextSibling;
        } else if (prevSibling.length > 0) {
            toFocus = (child !== null) ? prevSibling.find(child) : prevSibling;
        }
        return toFocus;
    }
    deleteReport = function (id) {
        $.getJSON('/reports/delete/' + id, function (data) {
            let moveTo = nextFocus('#report' + data.id, 'a', '#finished');
            $('#report' + data.id).remove();
            moveTo.focus();
        });
    }
    exportReport = function (id) {
        $.getJSON('/reports/export/' + id, function (data) {
            console.log(data);
            var blob = new Blob([JSON.stringify(data)], { type: "application/json" });
            var anchor = document.createElement("a");
            anchor.download = `report-${id}.json`;
            anchor.href = window.URL.createObjectURL(blob);
            anchor.target = "_blank";
            anchor.style.display = "none"; // just to be safe!
            document.body.appendChild(anchor);
            anchor.click();
            document.body.removeChild(anchor);
        });
    }
    cancelScan = function (id) {
        console.log("deleted " + id);
        showMessage('Cancelling Test Run', 'alert-warning');
        let moveTo = nextFocus('#report' + id, null, '#processing');
        $('#report' + id).remove();
        moveTo.focus();
        socket.emit('cancelScan', id);
    }
    socket.on('cancelScan', id => {
        if ($("#updates").is(':checked')) {
            console.log("deleted " + id);
            $(`#report${id}`).remove();
            showMessage('Test Run Cancelled', 'alert-warning');
        }
    });


    socket.on("startScanning", id => {
        console.log("startScanning");
        if ($("#updates").is(':checked')) {
            $.getJSON(`/reports/row/get/${id}`, ({ id, time, reportName }) => {

                $("#processing tbody").append(`
                    <tr id="report${id}">
                        <th scope="row">${time}</th>
                        <td>${reportName}</td>
                        <td class="processing">Processing</td>
                        <td><button onclick="cancelScan(${id})" 
                            data-toggle="tooltip" 
                            data-placement="top"
                            title="Cancel scan and shift focus"
                            class="btn btn-warning btn-sm"><span
                                    class="fas fa-stop"></span> Cancel <span class="sr-only">scan started
                                    ${time}</span></button></td>
                    </tr>`);
                showMessage(`New Test Run Started ${time}`);
            })
        }
    });
    socket.on("updateProgress", ({ runId, numRemaining }) => {
        if ($("#updates").is(':checked')) {
            $(`#processing tbody #report${runId} td.processing`).html(numRemaining);
        }
    })

    socket.on("reportFinished", id => {
        console.log("reportFinished")
        if ($("#updates").is(':checked')) {
            $.getJSON('/reports/row/get/' + id, ({ id, time, reportName, siteHealth }) => {
                $("#reports tbody").append(`
                <tr id="report${id}">
                    <th><a href="/reports/get/${id}">${time}</a></th>
                    <td>${reportName}</td>
                    <td>${siteHealth}%</td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <div class="btn-group">
                                <button onclick="exportReport(${id})" 
                                data-toggle="tooltip" 
                                data-placement="top"
                                title="Export results to JSON file"
                                class="btn btn-primary btn-sm"><span
                                    class="fas fa-file-export"></span> Export <span class="sr-only">scan started
                                    ${time}</span></button>
                            </div>
                            <div class="btn-group">
                                <button onclick="deleteReport(${id})" 
                                    data-toggle="tooltip" 
                                    data-placement="top"
                                    title="Delete and shift focus"
                                    class="btn btn-danger btn-sm"><span
                                    class="fas fa-trash-alt"></span> Delete <span class="sr-only">scan started
                                    ${time}</span></button>
                            </div>
                        </div>
                    </td>
                </tr>`).find('[data-toggle="tooltip"]').tooltip();
                $(`#report${id} th:first-child a`).focus();

                $(`#processing #report${id}`).remove();
                showMessage('Test Run Finished', 'alert-success');
            })
        }
    })

})
function startScan(data) {

    $("#cumulative").hide();
    // Makes it so labels are unique for each chart
    Highcharts.setOptions({
        lang: {
            accessibility: {
                screenReaderSection: {
                    beforeRegionLabel: 'Chart screen reader information, {chart.options.title.text}'
                },
                exporting: {
                    exportRegionLabel: "Chart menu, {chart.options.title.text}"
                }
            }
        }
    });

    var socket = io('//' + document.location.hostname + ':' + document.location.port);
    let status = 'processing';

    let currentScan;

    const addZero = (i) => {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    const timeLogged = () => {
        let d = new Date();
        let h = addZero(d.getHours());
        let m = addZero(d.getMinutes());
        let s = addZero(d.getSeconds());
        let ms = d.getMilliseconds()
        return `${h}:${m}:${s}.${ms}`;
    }

    const showMessage = (msg, alertClass = "alert-info") => {
        $('#messages').append(`<p class="alert ${alertClass}">${msg}</p>`).find('p:last-child').delay(3000).fadeTo(400, 0, function () { this.remove(); });
        $('#log .list-group').append(`<li class="list-group-item list-group-item-info"><b>${timeLogged()} &gt;</b> ${msg}</li>`);
    }
    const error = msg => {
        $('#alerts').append(`<p class="alert alert-danger">${msg}</p>`).find('p:last-child').delay(3000).fadeTo(400, 0, function () { this.remove(); });
        console.error(msg);
        $('#log .list-group').append(`<li class="list-group-item list-group-item-danger"><b>${timeLogged()} &gt;</b> ${msg}</li>`);
    }

    cancelScan = function () {
        status = 'cancelled';
        socket.emit('cancelScan', currentScan);
        $("#cancelScan").hide();
        $("#accordion").empty();
        $("#stats").empty();
        $("#wcag").empty();
    }
    socket.emit("startScanning", {
        scanId: data.id
    });
    socket.on("displayConfig", ({ config, runId, numActive }) => {
        currentScan = runId;
        console.log(numActive);
        $('#progress').attr('aria-valuemax', `${numActive}`);

        let valuetext = `0 of ${numActive}`;
        $('#progress').attr('aria-valuenow', `0`);
        $('#progress').attr('aria-valuetext', `${valuetext}`);
        $('#progress').attr('style', `width: 0%`);
        $('#progress').html(valuetext);
        $("#loading").removeClass('hidden');

        socket.emit("scanSite", { scanId: data.id, runId: runId });
        showMessage(`Beginning Test Run`);
        let conf = Handlebars.templates.config(config);
        $("#config").append(conf);
    });
    socket.on("cancelScan", scanId => { //this is if cancelled by /reports
        console.log("Cancel SCAN");
        $("#cancelScan").hide();
        status = "cancelled";
        let valuetext = $("#progress").text() + ' (cancelled)';
        $("#progress").removeClass('progress-bar-striped');
        $("#progress").addClass('progress-bar-danger');
        $("#progress").html(valuetext);
        $('#progress').attr('aria-valuetext', `${valuetext}`);
        showMessage('Test Run Cancelled', 'alert-warning');
        $("#accordion").empty();
        $("#stats").empty();
        $("#wcag").empty();
    });
    socket.on('updateProgress', data => {
        let valuemax = parseInt($('#progress').attr('aria-valuemax'));
        let valuenow = valuemax - data.numRemaining;
        let pct = (valuenow / valuemax) * 100;
        let valuetext = `${valuenow} of ${valuemax}`;
        $('#progress').attr('aria-valuenow', `${valuenow}`);
        $('#progress').attr('aria-valuetext', `${valuetext}`);
        $('#progress').attr('style', `width: ${pct}%`);
        $('#progress').html(valuetext);
    });
    socket.on('displayPageHead', data => {
        console.log('displayPageHead');
        console.log(data);
        // let valuemax =  parseInt($('#progress').attr('aria-valuemax'));
        // let valuenow = valuemax - data.numRemaining;
        // let pct = (valuenow/valuemax)*100;
        // let valuetext = `${valuenow} of ${valuemax}`;
        // $('#progress').attr('aria-valuenow', `${valuenow}`);
        // $('#progress').attr('aria-valuetext', `${valuetext}`);
        // $('#progress').attr('style', `width: ${pct}%`);
        // $('#progress').html(valuetext);
        displayPageHead(data);
    });
    socket.on('setCumulative', ({ stats, wcag }) => {
        $("#cancelScan").hide();
        $("#progress").removeClass('progress-bar-striped');
        $("#progress").addClass('progress-bar-success');
        showMessage('Finished Test Run', 'alert-success');
        let siteStats = compiledSiteStatsTemplate(stats)
        $("#stats").prepend(siteStats);

        let wcagConf = getWcagConf(wcag);
        Highcharts.chart('wcag', wcagConf);
        $("#cumulative").show();
    });
    socket.on("startRemoveFromCancelled", scanId => {
        socket.emit("removeFromCancelled", scanId);
    });
    socket.on("scanDeleted", scanId => {
        $("#accordion").empty();
        $("#stats").empty();
        $("#wcag").empty();
    });

    socket.on('error', ({ msg }) => {
        error(msg);
    });

    //////////////////////////////////

    socket.on('viewViolationLength', data => {
        let i = data.viewNum;
        let violations = data.violations;
        Highcharts.chart('violationsChart' + i, getViolationsConf(violations));
        console.log("violations length");
    });
    socket.on('viewViolationInstancesLength', data => {
        let i = data.viewNum;
        let instances = data.instances;
        Highcharts.chart('issuesChart' + i, getIssuesConf(instances));
        console.log("instances length");
    });
    socket.on('viewStats', data => {
        let i = data.viewNum;
        let panelSelector = "#panel" + i;
        let pageTabPanelSelector = panelSelector + " #collapse" + i;
        let pageBodySelector = pageTabPanelSelector + " .panel-body";
        var stats = compiledStatsTemplate(data);
        $(pageBodySelector).prepend(stats);
        console.log("instances stats");
    });
    socket.on('violations', data => {
        console.log(data);
        let i = data.viewNum;
        let violations = data.violations;
        let panelSelector = "#panel" + i;
        let pageTabPanelSelector = panelSelector + " #collapse" + i;
        let pageBodySelector = pageTabPanelSelector + " .panel-body";
        let violationsSelector = pageBodySelector + " .violations";
        let parsedViolations = violations.map(function (rule, v) {

            if (rule.impact === 'critical') {
                variation = 'danger';
                icon = 'skull-crossbones';
            } else if (rule.impact === 'serious') {
                variation = 'warning';
                icon = 'radiation-alt';
            } else if (rule.impact === 'moderate') {
                variation = 'info';
                icon = 'exclamation-triangle';
            } else if (rule.impact === 'minor') {
                variation = 'success';
                icon = 'bell';
            }

            return {
                impact: rule.impact,
                variation: variation,
                icon: icon,
                help: rule.help,
                description: rule.description,
                helpUrl: rule.helpUrl,
                count: rule.nodes.length,
                index: v,
                nodes: rule.nodes.map((node, n) => {

                    ({ target, html, failureSummary, any, all, none } = node);

                    html = compiledCodeTemplate({
                        html: Prism.highlight(html, Prism.languages.html, 'html')
                    });
                    target = compiledTargetsTemplate({ targets: target });

                    fixId = "fix" + i + "_" + v + "_" + n
                    reasonHtml = summary(node, fixId);

                    return {
                        target, html, reasonHtml, failureSummary, index: n
                    }
                })
            };

        });

        let myViolations = compiledViolationsTemplate({ violationList: parsedViolations });
        $(violationsSelector).html(myViolations);
    });

    function messageFromRelatedNodes(relatedNodes) {
        var retVal = '';
        if (relatedNodes.length) {
            var list = relatedNodes.map(function (node) {
                return {
                    targetArrayString: JSON.stringify(node.target),

                    targetString: compiledTargetsTemplate({
                        targets: node.target
                    }),
                    htmlString: compiledCodeTemplate({
                        html: Prism.highlight(node.html, Prism.languages.html, 'html')
                    })
                };
            });
            retVal += compiledRelatedListTemplate({ relatedNodeList: list });
        }
        return retVal;
    }

    function messagesFromArray(nodes) {
        var list = nodes.map(function (failure) {
            return {
                message: failure.message,// failure.message.replace(/</gi, '&lt;').replace(/>/gi, '&gt;'),
                relatedNodesMessage: messageFromRelatedNodes(failure.relatedNodes)
            }
        });
        return compiledReasonsTemplate({ reasonsList: list });
    }

    function summary(node, fix) {
        var toggle = `
            <span class="fas fa-plus-square" aria-hidden="true"></span><span class="fas fa-minus-square" aria-hidden="true"></span> `
        var retVal = `
            <button 
                class="button button-primary" 
                type="button" data-toggle="collapse" 
                data-target="#${fix}" 
                aria-expanded="false" 
                aria-controls="${fix}">
                    ${toggle}How to fix
            </button> 
            <div class="collapse" id="${fix}">`;
        if (node.any.length) {
            retVal += '<h6 class="error-title">Fix any of the following</h6>';
            retVal += messagesFromArray(node.any);
        }

        var all = node.all.concat(node.none);
        if (all.length) {
            retVal += '<h6 class="error-title">Fix all of the following</h6>';
            retVal += messagesFromArray(all);
        }
        retVal += "</div>"
        return retVal;
    }

    function initPanel(evt) {
        let controls = $(evt.target).attr('aria-controls');
        let i = controls.replace("collapse", "");

        let params = { scanId: currentScan, viewNum: i }
        console.log(params)
        socket.emit('getViewViolationLength', params);
        socket.emit('getViewViolationInstancesLength', params);
        socket.emit('getViewStats', params);
        socket.emit('getViolations', params);

        $(evt.target).off("." + controls);
    }
    function helperItemIterator(items, template) {
        var out = '';
        if (items) {
            for (var i = 0; i < items.length; i++) {
                out += template(items[i]);
            }
        }
        return out;
    }
    Handlebars.registerHelper('violations', items => {
        return helperItemIterator(items, compiledViolationTemplate);
    });
    Handlebars.registerHelper('related', items => {
        return helperItemIterator(items, compiledRelatedNodeTemplate);
    });
    Handlebars.registerHelper('reasons', items => {
        return helperItemIterator(items, compiledFailureTemplate);
    });
    Handlebars.registerHelper('insts', items => {
        return helperItemIterator(items, compiledInstTemplate);
    });
    Handlebars.registerPartial("instances", instancesTemplate.innerHTML);

    compiledSiteStatsTemplate = Handlebars.templates.siteStats;
    compiledTargetsTemplate = Handlebars.templates.targets;
    compiledCodeTemplate = Handlebars.templates.code;

    compiledViolationTemplate = Handlebars.templates.violation;
    compiledViolationsTemplate = Handlebars.templates.violations;

    compiledPagePanelTemplate = Handlebars.templates.pagePanel;
    compiledPageTabPanelTemplate = Handlebars.templates.pageTabPanel;
    compiledPageHeadTemplate = Handlebars.templates.pageHead;
    compiledChartsTemplate = Handlebars.templates.charts;
    compiledPageBodyTemplate = Handlebars.templates.pageBody;
    compiledRulePanelTemplate = Handlebars.templates.rulePanel;

    compiledRelatedListTemplate = Handlebars.templates.relatedList;
    compiledRelatedNodeTemplate = Handlebars.templates.relatedNode;
    compiledFailureTemplate = Handlebars.templates.failure;
    compiledReasonsTemplate = Handlebars.templates.reasons;

    compiledStatsTemplate = Handlebars.templates.stats;

    displayPageHead = head => {
        let panelSelector = "#panel" + head.i;
        let pageTabPanelSelector = panelSelector + " #collapse" + head.i;
        let pageBodySelector = pageTabPanelSelector + " .panel-body";

        let pagePanel = compiledPagePanelTemplate({ i: head.i }); //$(#panel\{{i}})
        let pageHead = compiledPageHeadTemplate(head); //#panel\{{i}} #heading\{{i}}
        var pageTabPanel = compiledPageTabPanelTemplate({ i: head.i, in: '' }); //#panel\{{i}} #collapse\{{i}}
        var pageBody = compiledPageBodyTemplate(); //#panel\{{i}} #collapse\{{i}} .panel-body
        var charts = compiledChartsTemplate({ i: head.i }); //#panel\{{i}} #heading\{{i}} .panel-body container

        $("#accordion").append(pagePanel);
        $(panelSelector).prepend(pageHead);
        $('[aria-controls="collapse' + head.i + '"]').on("click.collapse" + head.i, initPanel);
        $(panelSelector).append(pageTabPanel);
        $(pageTabPanelSelector).append(pageBody);
        $(pageBodySelector).prepend(charts);
    }

}
$(document).ready(function () {
    var socket = io('//' + document.location.hostname + ':' + document.location.port);
    const addZero = (i) => {
        if (i < 10) {
          i = "0" + i;
        }
        return i;
    }
    const timeLogged = () => {
        let d = new Date();
        let h = addZero(d.getHours());
        let m = addZero(d.getMinutes());
        let s = addZero(d.getSeconds());
        return h + ":" + m + ":" + s;
      }
    
    const showMessage = (msg, alertClass="alert-info") => {
        $('#messages').append(`<p class="alert ${alertClass}">${msg}</p>`).find('p:last-child').delay(3000).fadeTo(400, 0, function() { this.remove(); });
        $('#log .list-group').append(`<li class="list-group-item list-group-item-info"><b>${timeLogged()} &gt;</b> ${msg}</li>`);
    }
    const error = msg => {
        $('#alerts').append(`<p class="alert alert-danger">${msg}</p>`).find('p:last-child').delay(3000).fadeTo(400, 0, function() { this.remove(); });
        console.error(msg);
        $('#log .list-group').append(`<li class="list-group-item list-group-item-danger"><b>${timeLogged()} &gt;</b> ${msg}</li>`);
    }

    socket.on('error', ({msg}) => {
        error(msg);
    });

    $('[data-toggle="tooltip"]').tooltip();
    let itemType = "/scans/scan/";
    nextFocus = function(sel, child, parentEl) {
        let nextSibling = $(sel).next();
        let prevSibling = $(sel).prev();
        let toFocus = $(parentEl);
        console.log(nextSibling);
        if (nextSibling.length > 0) {
            toFocus = (child !== null) ? nextSibling.find(child) : nextSibling;
        } else if (prevSibling.length > 0) {
            toFocus = (child !== null) ? prevSibling.find(child) : prevSibling;
        }
        return toFocus;
    }
    duplicateScan = function(id) {
        $.getJSON(itemType+'duplicate/'+id, function (data) {
            let row =  `
                <tr id="item${data.id}">
                    <th scope="row"><a href="/scans/scan/update/${data.id}">${data.reportName}</a></th>
                    <td>Not Run Yet</td>
                    <td style="max-width:10em; min-width:8em">
                        <div class="btn-group btn-group-justified">
                            <div class="btn-group"><a href="/scans/scan/run/${data.id}"
                                    data-toggle="tooltip" 
                                    data-placement="top"
                                    title="Runs in new page"
                                    class="btn btn-primary btn-sm"><span class="fas fa-running"> </span> <span
                                        class="link visible-md-inline visible-lg-inline">Run</span></a></div>
                            <div class="btn-group"><button onclick="duplicateScan(${data.id})"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Duplicates and shifts focus"
                                    class="btn btn-success btn-sm"><span class="fas fa-clone"></span> <span class="visible-md-inline visible-lg-inline">Clone</span></button></div>
                            <div class="btn-group"><button onclick="deleteScan(${data.id})"
                                    data-toggle="tooltip" 
                                    data-placement="top"
                                    title="Deletes and shifts focus"
                                    class="btn btn-danger btn-sm"><span class="fas fa-trash-alt"></span> <div class="visible-md-inline visible-lg-inline">Delete</div></button>
                            </div>
                        </div>
                    </td>
                </tr>
                `;
            $('#items tbody').append(row).find('[data-toggle="tooltip"]').tooltip();
            $(`#item${data.id} th a:first-child`).focus();
        });
    }
    deleteScan = function(id) {
        console.log(id);
        showMessage('Deleting Scan');
        $.getJSON(itemType+'delete/'+id, function (data) {
            let moveTo = nextFocus('#item'+data.id, 'th a', '#confCaption');
            $('#item'+data.id).remove();
            $('#msg').remove();
            moveTo.focus();
        });
    }
})